import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Video } from './dashboard/interfaces';
import { map } from 'rxjs/operators';

const url = 'https://api.angularbootcamp.com/videos';

@Injectable({
  providedIn: 'root'
})
export class VideoDataService {
  constructor(private http: HttpClient) {}

  getVideo(id: string): Observable<Video> {
    return this.http.get<Video>(`${url}/${id}`);
  }

  loadVideos(): Observable<Video[]> {
    return this.http.get<Video[]>(url).pipe(
      map(videos =>
        videos
          .filter(video => {
            return video.title.startsWith('Angular');
          })
          .map(video => {
            return {
              ...video,
              title: video.title.toUpperCase()
            };
          })
      )
    );
  }
}
