import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Video } from '../interfaces';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-video-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent {
  @Input() videos: Video[] = [];
  selectedId: Observable<string>;

  constructor(route: ActivatedRoute, private router: Router) {
    this.selectedId = route.queryParamMap.pipe(
      map(params => params.get('selectedId'))
    );
  }

  select(video: Video) {
    const queryParams = {
      selectedId: video.id
    };
    this.router.navigate([], { queryParams });
  }
}
