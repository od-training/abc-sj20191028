import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { VideoDataService } from 'src/app/video-data.service';
import { Video } from '../interfaces';
import {
  Router,
  ActivatedRoute,
  ActivatedRouteSnapshot
} from '@angular/router';

@Component({
  selector: 'app-video-dashboard',
  templateUrl: './video-dashboard.component.html',
  styleUrls: ['./video-dashboard.component.scss']
})
export class VideoDashboardComponent {
  videos: Observable<Video[]>;

  constructor(
    service: VideoDataService,
    router: Router,
    route: ActivatedRoute
  ) {
    this.videos = service.loadVideos().pipe(
      tap(videos => {
        if (
          videos &&
          videos.length &&
          !route.snapshot.queryParamMap.get('selectedId')
        ) {
          const queryParams = { selectedId: videos[0].id };
          router.navigate([], { queryParams });
        }
      })
    );
  }
}
